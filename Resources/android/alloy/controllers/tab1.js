function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function switchTab(_evt) {
        App.tabgroup.setActiveTab(1);
        App.controllers[1].table.selectRow(_evt.index);
        App.controllers[1].win.setTitle(_evt.rowData.title);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "tab1";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        id: "win",
        title: "Tab 1"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.table = Ti.UI.createTableView({
        id: "table"
    });
    $.__views.win.add($.__views.table);
    switchTab ? $.__views.table.addEventListener("click", switchTab) : __defers["$.__views.table!click!switchTab"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var App = require("core");
    arguments[0] || {};
    $.table.data = App.data.table1Data;
    __defers["$.__views.table!click!switchTab"] && $.__views.table.addEventListener("click", switchTab);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;