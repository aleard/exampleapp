var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

var App = require("core");

var file = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory, "data.json");

if (file.exists()) {
    var dataSrc = "android" === Ti.Platform.osname ? "" + file.read() : file.read();
    App.data = JSON.parse(dataSrc);
}

Alloy.createController("index");