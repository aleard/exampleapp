function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    var __alloyId0 = [];
    $.__views.tab1 = Alloy.createController("tab1", {
        id: "tab1"
    });
    $.__views.__alloyId1 = Ti.UI.createTab({
        window: $.__views.tab1.getViewEx({
            recurse: true
        }),
        title: "Tab 1",
        icon: "KS_nav_ui.png",
        id: "__alloyId1"
    });
    __alloyId0.push($.__views.__alloyId1);
    $.__views.tab2 = Alloy.createController("tab2", {
        id: "tab2"
    });
    $.__views.__alloyId2 = Ti.UI.createTab({
        window: $.__views.tab2.getViewEx({
            recurse: true
        }),
        title: "Tab 2",
        icon: "KS_nav_views.png",
        id: "__alloyId2"
    });
    __alloyId0.push($.__views.__alloyId2);
    $.__views.tab3 = Alloy.createController("tab3", {
        id: "tab3"
    });
    $.__views.__alloyId3 = Ti.UI.createTab({
        window: $.__views.tab3.getViewEx({
            recurse: true
        }),
        title: "Tab 3",
        icon: "KS_nav_views.png",
        id: "__alloyId3"
    });
    __alloyId0.push($.__views.__alloyId3);
    $.__views.index = Ti.UI.createTabGroup({
        tabs: __alloyId0,
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var App = require("core");
    $.index.open();
    App.tabgroup = $.index;
    App.controllers = [ $.tab1, $.tab2, $.tab3 ];
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;